#include <vector>
#include <iostream>

// sparse matrix storage for a square n*n matrix
// supports both COO and CSR storage (both 0-based)
//
// COO: row_index and col_index give (i,j) pair + values
// CSR: row_start[i] is start index of row i
//      col_index + values give the column+value of each non-zero entry
//
// an additional vector gives an index
struct Matrix {
    std::vector<int> row_index; // length nnz
    std::vector<int> col_index; // length nnz
    std::vector<int> row_start; // length n+1
    std::vector<double> values; // length nnz

    // 0-based index for the diagonals
    std::vector<int> diagonals; // length n

    // assume that matrix is square
    int nnz;
    int n;

    Matrix add_nonlinear(std::vector<double>& u, double dts) {
        Matrix other = *this;
        for(int i=0; i<n; ++i) {
            other.values[diagonals[i]] += dts*(1.0 - 2.0*u[i]);
        }
        return other;
    }
};

int point_to_index(int i, int j, int nx, int ny) {
    if(i<0 || j<0 || i>(nx-1) || j>(ny-1)) {
        return -1;
    }
    return i + j*nx;
}

Matrix generate_linear_part(int nx, int ny, double alpha) {
    int n = nx*ny;

    int nz = 5*(nx-2)*(ny-2) + 4*2*(nx-2) + 4*2*(ny-2) + 4*3;
    std::cout << "generating matrix " << n << "x" << n << " with " << nz << " nonzeros" << std::endl;

    Matrix J;
    J.nnz = nz;
    J.n = n;
    J.values.reserve(nz);
    J.col_index.reserve(nz);
    J.row_index.reserve(nz);

    J.row_start.reserve(n);
    J.row_start.push_back(0);

    for(int j=0; j<ny; ++j) {
        for(int i=0; i<nx; ++i) {
            int idx;
            // south
            if((idx = point_to_index(i, j-1, nx, ny)) >= 0) {
                J.col_index.push_back(idx);
                J.values.push_back(1.0);
            }
            // west
            if((idx = point_to_index(i-1, j, nx, ny)) >= 0) {
                J.col_index.push_back(idx);
                J.values.push_back(1.0);
            }
            // center
            J.diagonals.push_back(J.values.size()); // store position of diagonal
            idx = point_to_index(i-1, j, nx, ny);
            J.col_index.push_back(idx);
            J.values.push_back(-(4. + alpha));
            // east
            if((idx = point_to_index(i+1, j, nx, ny)) >= 0) {
                J.col_index.push_back(idx);
                J.values.push_back(1.0);
            }
            // north
            if((idx = point_to_index(i, j+1, nx, ny)) >= 0) {
                J.col_index.push_back(idx);
                J.values.push_back(1.0);
            }
            J.row_start.push_back(J.values.size());
        }
    }

    return J;
}

int main(void) {
    int nx = 4;
    int ny = 4;
    int n = nx*ny;

    Matrix A = generate_linear_part(nx, ny, 0.2);

    std::vector<double> u(n, 0.05);
    Matrix J = A.add_nonlinear(u, 2.0);

    for(int i=0; i<A.n; ++i) {
        int row_start = A.row_start[i];
        int row_end   = A.row_start[i+1];
        for(int j=row_start; j<row_end; ++j) {
            std::cout << A.values[j] << " ";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
    for(int i=0; i<A.n; ++i) {
        int row_start = A.row_start[i];
        int row_end   = A.row_start[i+1];
        for(int j=row_start; j<row_end; ++j) {
            std::cout << J.values[j] << " ";
        }
        std::cout << std::endl;
    }
}
