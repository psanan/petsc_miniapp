/* modified from KSP tutorial ex50 */
/* This code simply assembles a matrix, but an be extended to use a linear (KSP) solver
or a nonliner (SNES) solver */
static char help[] = "Generate an example FD system.\n\n";

#include <petscdm.h>
#include <petscdmda.h>
#include <petscksp.h>
#include <petscsys.h>
#include <petscvec.h>

extern PetscErrorCode ComputeJacobian(DM,Vec,Mat,void*);

typedef enum {DIRICHLET, NEUMANN} BCType;

typedef struct {
  PetscScalar alpha, dts;
} UserContext;

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  DM             da;
  Mat            J;
  Vec            u;
  UserContext    user;
  PetscInt       bc;
  PetscErrorCode ierr;

  float removeThis;

  PetscInitialize(&argc,&argv,(char*)0,help);
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_STAR,-4,-4,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);

  user.alpha   = 0.2;
  user.dts     = 2.0;

  ierr = DMCreateMatrix(da,&J);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&u);CHKERRQ(ierr);

  ierr = VecSet(u,0.05);CHKERRQ(ierr);

  ierr = ComputeJacobian(da,u,J,&user);CHKERRQ(ierr);

  ierr = MatView(J,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);

  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&u);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);

  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "ComputeJacobian"
PetscErrorCode ComputeJacobian(DM da,Vec u,Mat J,void *ctx)
{
  UserContext       *user = (UserContext*)ctx;
  PetscErrorCode    ierr;
  PetscInt          i, j, M, N, xm, ym, xs, ys, num;
  PetscScalar       v[5];
  const PetscScalar *uarr;
  MatStencil        row, col[5];

  PetscFunctionBeginUser;
  ierr = VecGetArrayRead(u,&uarr);CHKERRQ(ierr);
  ierr  = DMDAGetInfo(da,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  ierr  = DMDAGetCorners(da,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);

  /* Print the local sizes */
  #if 0
  {
    PetscMPIInt rank,size;
    ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
    ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
    ierr = PetscSynchronizedPrintf(PETSC_COMM_WORLD,"proc id %d [%d total]. xs:%d ys:%d xm:%d ym:%d\n",rank,size,xs,ys,xm,ym);CHKERRQ(ierr);
    ierr = PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);CHKERRQ(ierr);
  }
  #endif

  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      row.i = i; row.j = j;
      v[0] = -(4.0 + user->alpha) + user->dts*(1.0 - 2.0*uarr[i]); 
      col[0].i = i; 
      col[0].j = j;
      num = 1;
      if (j!=0) {
        v[num] = 1.0;                
        col[num].i = i;   
        col[num].j = j-1;
        ++num;
      }
      if (i!=0) {
        v[num] = 1.0;                
        col[num].i = i-1; 
        col[num].j = j;
        ++num;
      }
      if (i!=M-1) {
        v[num] = 1.0;                
        col[num].i = i+1; 
        col[num].j = j;
        ++num;
      }
      if (j!=N-1) {
        v[num] = 1.0;                
        col[num].i = i;   
        col[num].j = j+1;
        ++num;
      }
      ierr = MatSetValuesStencil(J,1,&row,num,col,v,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = VecRestoreArrayRead(u,&uarr);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

