A simple example of assembling a finite difference matrix in parallel with PETSc

To use, 

- install PETSc and set `PETSC_DIR` and `PETSC_ARCH` in your environment. See http://www.mcs.anl.gov/petsc/documentation/installation.html for full instructions, but this might be as simple as
    - `git clone https://bitbucket.org/petsc/petsc -b maint petsc`
    - `cd petsc`
    - `./configure --with-cc=gcc --with-cxx=g++ --with-fc=gfortran --download-fblaslapack --download-mpich`
    - Follow the instructions to make and test
    - `export PETSC_DIR=$PWD`
    - `export PETSC_ARCH=<the arch defined during configuration>`
- `make`
- `make runRef`
- `make run`, and confirm that the same matrix is generated