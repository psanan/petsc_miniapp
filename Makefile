include ${PETSC_DIR}/lib/petsc/conf/petscvariables

REF_EXNAME=runMeRef
REF_OBJ=matrix.o

EXNAME=runMe
OBJ=main.o

LIB=$(PETSC_LIB)
INCLUDE=$(PETSC_CC_INCLUDES)

all : $(REF_EXNAME) $(EXNAME)

$(REF_EXNAME) : $(REF_OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(EXNAME) : $(OBJ)
	$(CXX) $(CFLAGS) $(LIB) -o $@ $^ 

%.o : %.cpp
	$(CXX) -c $< -o $@

%.o : %.c
	$(CC) $(INCLUDE) -c $< -o $@

clean :
	rm -f $(REF_OBJ) $(REF_EXNAME) $(OBJ) $(EXNAME)

runRef : $(REF_EXNAME)
	./$(REF_EXNAME)

run : $(EXNAME)
	$(MPIEXEC) -n 4 ./$(EXNAME)

.PHONY: run runRef all clean
